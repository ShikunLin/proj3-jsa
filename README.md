# proj3-JSA

Author: Shikun Lin  
Contact address: shikunl@uoregon.edu

## Description

A simple anagram game designed for English-language learning students in elementary and middle school. Students are presented with a list of vocabulary words (taken from a text file) and an anagram. The anagram is a jumble of some number of vocabulary words, randomly chosen. Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words. 

The vocabulary word list is fixed for one invocation of the server, so multiple students connected to the same server will see the same vocabulary list but may  have different anagrams.

## How to Run  

1. Open the terminal, then cd to "vocab" directory  
2. Put your credentials.ini file into "vocab" director  
3. Build the image and container with "./run.sh"  
4. Launch [http://127.0.0.1:5000](http://127.0.0.1:5000) using web browser  
